# Horizontal scaling: adding containers to existing VM(s)

## via gitops

### set replicas
```
  frontend:
    image: bobrohan/dreamteam-web:latest
    ports:
      - "80:8089"
    depends_on:
      - backend
    deploy:
      replicas: 2
```

### deploy stack
`docker stack deploy --compose-file /dream-team/docker-compose.yml dreamteam`

*OR*

## via cli
docker service scale frontend=2