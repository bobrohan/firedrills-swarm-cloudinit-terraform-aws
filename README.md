# Fire drills

Collection of READMEs for various fire drill scenarios

- [node-availability-senarios](./node-availability-senarios.md)
- [replicas-scenarios](./replicas-scenarios.md)
- [logs-reading-scenarios](./logs-reading-scenarios.md)