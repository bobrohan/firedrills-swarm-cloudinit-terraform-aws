# Drain misbehaving node

## drain node
docker node update --availability drain HOSTNAME

## confirm node availability
docker node inspect --pretty ud8usnyn4aifbmmywajui7vuz

```
Status:
 State:                 Ready
 Availability:          Active
```

# Activate replacement node

## join node
docker swarm join --token $TOKEN $MANAGER_IP:2377

## activate node
docker node update --availability activate HOSTNAME

## distribute replicas
docker service update $1 --force --detach=false